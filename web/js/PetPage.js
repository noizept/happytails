$().ready(function() {

    /***
     *  1 - Inicia Galeria de fotos
     *  2 - Butao de report, ao clickar faz popup se ker mesmo report e efetua AJAX call com um report
     *  3 - butao de contactar,  ______________________________________________________________ ação de contato
     */

    document.getElementById('links').onclick = function (event) { //1
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {
                index: link,
                event: event
            },
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };

    $('#pet-report-btn').on('click',function(event){ //2
            bootbox.dialog({
                message: "You are about to report this profile. Are you sure?",
                title: "Report notice",
                buttons: {
                    success: {
                        label: "Yes",
                        className: "btn-success",
                        callback: function() {
                            var lasturl=location.pathname.split('/').pop();
                            $.ajax({
                                method: "POST",
                                url: "/mail/report",
                                data: { idPet: lasturl }
                            })
                                .done(function( msg ) {
                                    bootbox.dialog({
                                        message: "Email sent!",
                                        title: "Report notice",
                                        buttons: {
                                            success: {
                                                label: "Success!",
                                                className: "btn-success"
                                            }}})
                                });
                        }
                    },
                    danger: {
                        label: "Cancel",
                        className: "btn-danger"
                    }
                }
            });
    });
    $('#pet-contact-btn').on('click',function(e){ //3
        bootbox.dialog({
            message: "By agreeing your email will be sent to the current user. Any further actions Happy" +
            "Tails do not take any responsability.",
            title: "Agreement notice",
            buttons: {
                success: {
                    label: "I Agree!",
                    className: "btn-success",
                    callback: function() {
                        var lasturl=location.pathname.split('/').pop();
                        $.ajax({
                            method: "POST",
                            url: "/mail/contact",
                            data: { idPet: lasturl }
                        })
                            .done(function( msg ) {
                                bootbox.dialog({
                                    message: "Email sent!",
                                    title: "Agreement notice",
                                    buttons: {
                                        success: {
                                            label: "Success!",
                                            className: "btn-success"
                                        }}})
                            });
                    }
                },
                danger: {
                    label: "I Disagree!",
                    className: "btn-danger",
                }
            }

        });
    });



});