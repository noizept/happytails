$().ready(function() {
    $("#petbreed").on('change',function(event){
    $.ajax(
        {
            url: "/request/getall",
            type: "POST",
            data: $("#requestform").serializeArray(),
            success: function (data, textStatus, jqXHR) {
                $('#listagem').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //if fails     
            }
        });
    return false;
    });

    $('body').on('click', '.contact', function() {
        var id=$(this).val();
        bootbox.dialog({
            message: "You are about to Contact the user. Are you sure?",
            title: "Notice",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            method: "POST",
                            url: "/mail/request",
                            data: {'requestid':id }
                        })
                            .done(function( msg ) {
                                bootbox.dialog({
                                    message: "Email sent",
                                    title: "Report notice",
                                    buttons: {
                                        success: {
                                            label: "Success!",
                                            className: "btn-success"
                                        }}})
                            });
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-danger"
                }
            }
        });

    });


});