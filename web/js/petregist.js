/**
 * Created by sergio on 22-11-2015.
 */
/***
 * 1 - Inicia calendario
 * 2 - Validações de formulario
 * 3 - ajax call para submeter o pet
 */


$().ready(function() {
    $("#petbirth").datepicker(); //1
    $("#pet-insert").validate({ //2
        rules: {
            gender :{
                required : true,
                minlength:1,
                maxlength:1
            },
            petfamily :{
                required : true
            },
            petbreed :{
                required : true
            },
            petbirth :{
                required : true,
                date : true
            }
        },

        messages:{
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });


    $("#subm-btn").on('click',function(e){ //3
        var petForm=$("#pet-insert");


        if (!petForm.valid()) return false;
        var postData = petForm.serializeArray();
        $.ajax(
            {
                url: "/addpet",
                type: "POST",
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    $('#conteudo').html(data);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                }
            });
        return false;
    })

});