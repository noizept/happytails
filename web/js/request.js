/**
 * Created by sergio on 29-11-2015.
 * Popup e AJAX de pet request
 */
$().ready(function() {
    $('#makerequest').on('click', function (event) {
        event.preventDefault();
        bootbox.dialog({
            message: "You are about to make a Pet Request. Are you sure?",
            title: "Notice",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn-success",
                    callback: function () {
                        var formData = $("#requestform").serializeArray();
                        $.ajax({
                            method: "POST",
                            url: "/request/create",
                            data: formData
                        })
                            .done(function (msg) {
                                bootbox.dialog({
                                    message: "Request Added!",
                                    title: "Report notice",
                                    buttons: {
                                        success: {
                                            label: "Success!",
                                            className: "btn-success"
                                        }
                                    }
                                })
                            });
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-danger"
                }
            }
        });
    });
});