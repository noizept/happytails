/**
 * Created by sergio on 29-11-2015.
 */

/***
 * 1- Inicia a data como um Datepicker(Calendário)
 * 2- Inicia as photos como sendo uma galeria de foto
 * 3- Ao clickar no butao de submeter, verifica se info é vaálida e se sim mostra popup
 * a confirmar se realmente quer editar o pet, se sim faz AJAX call para modificar o mesmo
 *
 * 4- Se clickar remover/manter foto insere/remove do array de fotos a remover e tambem (des)foca imagem
 * 5 - Validações de formulario
 */
$().ready(function(){
    $("#petbirth").datepicker(); //1

    $(".links").on('click',function(event){ //2
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {
                index: link,
                event: event
            },
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);


    });

    $('#subm-btn').on('click',function(event){ //3
        var petForm=$("#pet-insert");
        if (!petForm.valid()) return false;

        bootbox.dialog({
            message: "You are about to change your pet information. Are you sure?",
            title: "Notice",
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn-success",
                    callback: function () {
                        var formData = petForm.serializeArray();
                        var searchIDs = $('[name=\'keepers[]\']').map(function () {
                            return $(this).val();
                        });
                        formData.push({name: "photos", value: $.unique(searchIDs.get())});
                        $.ajax({
                            url: "/validatechange",
                            type: "POST",
                            data: formData,
                            success: function (data, textStatus, jqXHR) {
                                console.log(data);

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //if fails     
                            }
                        }).done(function (msg) {
                            bootbox.dialog({
                                message: "Email sent!",
                                title: "Report notice",
                                buttons: {
                                    success: {
                                        label: "Success!",
                                        className: "btn-success"
                                    }
                                }
                            })
                        });
                    }
                },
                danger: {
                    label: "Cancel",
                    className: "btn-danger"
                }
            }
        });
    });


    $('.idbutton').on('click',function(e){ //4

        var val=$(this).val();
        var html=$(this).html();
        if(html=='Remove Photo') {
            $(this).html('Keep Photo');
            $("#photo"+val).foggy();
            $("#hidden"+val).val(val);
        }
        else {
            $(this).html('Remove Photo');
            $("#photo"+val).foggy(false);
            $("#hidden"+val).val(0);

        }
    });

    $("#pet-insert").validate({ //5
        rules: {
            gender :{
                required : true,
                minlength:1,
                maxlength:1
            },
            petfamily :{
                required : true
            },
            petbreed :{
                required : true
            },
            petbirth :{
                required : true,
                date : true
            }
        },

        messages:{
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

});