/**
 * Created by sergio on 28-11-2015.
 */

/***
 * Função de update no formulario para as raças dos pets
 */
$().ready(function() {

    $("#petfamily").change(function () {
        $('#petbreed')
            .find('option')
            .remove()
            .end();
        var Url =
            "http://api.petfinder.com/breed.list?format=json&" +
            "key=1ddf5a0681e8dc18b0725da9a667b8b5&animal=" + this.value +
            "&callback=?";
        $.getJSON(Url)
            .done(function (data) {
                $("#petbreed").append("<option disabled selected> -- select an option -- </option>");
                $.each(data.petfinder.breeds.breed, function (i, val) {
                    $.each(val, function (k, val1) {
                        var opt = "<option value='" + val1 + "'>" + val1 + "</option>";
                        $("#petbreed").append(opt)
                    })
                })
            })
            .error(function (err) {
                alert('Error retrieving data!');
            });


    })
});
