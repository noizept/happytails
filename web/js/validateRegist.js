/**
 * Created by sergio on 21-11-2015.
 */
/**
 * Validação de registo
 * Evento de submição
 *
 */
$().ready(function() {


    $("#registform")
        .validate({
            rules: {

                petfinder_email: {
                    required: true,
                    minlength: 8,
                    email: true
                },
                petfinder_password_confirm: {
                    equalTo: "#petfinder_password"
                },
                petfinder_password: {
                    required: true,
                    minlength: 8
                },
                petfinder_name: {
                    required: true,
                    minlength: 3
                },
                petfinder_city: {
                    required: true,
                    minlength: 4
                },
                petfinder_phone: {
                    required: true,
                    digits: true,
                    minlength: 9, maxlength: 9
                }
            },

            messages: {},
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

    $("#registform").on('submit', function (e) {
        if (!$("#registform").valid()) return false;
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
            {
                url: formURL,
                type: "POST",
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    $('#formcontent').html(data);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                }
            });
        return false;
    });
});