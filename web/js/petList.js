/**
 * Created by sergio on 28-11-2015.
 */
/***
 * 1,2 - Inicia um calendário no formulario
 * 3 - AJAX call com informção dos filtros e atualiza lista
 *
 */
$().ready(function() {
    $("#petbirth").datepicker(); //1
    $("#petbirth2").datepicker(); //2
    $("#petbreed, #petbirth, #petbirth2").on('change',function(event){ //3
        $.ajax(
            {
                url: "/listupdate",
                type: "POST",
                data: $("#searchform").serializeArray(),
                success: function (data, textStatus, jqXHR) {
                    $('#listagem').html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails     
                }
            });
        return false;
    });

});