<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class MainpageController extends Controller
{
    /**
     * @Route("/main", name="main_page")
     */
    public function indexAction(Request $request)
    {
        if($this->get('login_check')->checkLogin($request))
            return $this->render('Main/main.html.twig',
                array('signed'=>true));
        else
            return $this->redirectToRoute('homepage');

    }
}