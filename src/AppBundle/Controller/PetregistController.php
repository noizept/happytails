<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pet;
use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;
use \DateTime;


/**
 * Class PetregistController
 * @package AppBundle\Controller
 */
class PetregistController extends Controller
{
    /**
     * @Route("/registpet", name="pet_regist")
     */
    /***
     * Controlador responsavel por carregador o formulario de registo,
     * e apaga tambem os ficheiros temporarios se existirem de um upload anterior
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        if(!$this->get('login_check')->checkLogin($request)) return $this->redirectToRoute('homepage');
        $this->get('file_util')->fileDeleteTemp($request->getSession()->get('id'));
        return $this->render(':Pets:petregist.html.twig',array('signed'=>true));
    }

    /**
     * @Route("/registpetphoto", name="registpetphoto")
     */
    /***
     * Guarda o ficheiro recebido na pasta temporarária uploads
     * @param Request $request
     * @return JsonResponse
     */
    public function getPhoto(Request $request){
        if(empty($request->files->get('file'))) return new JsonResponse([]);
        $this->get('file_util')->fileToTemp($request->files->get('file'),
                                            $request->getSession()->get('id'));
        return new JsonResponse([]);
    }

    /**
     * @Route("/addpet", name="pet_add")
     */
    /*** Verifica login, testa a os dados do formulario , insere os dados na BD
     * Se Existir photos faz upload para a Cloud
     * Insere na BD das fotos e apagar os temporarios
     */
    public function petAdd(Request $request)
    {

        if (!$this->get('login_check')->checkLogin($request)) return new Response();
        $em = $this->getDoctrine();
        $pet = $this->valid_Pet($request, $em);
        $fileControl = $this->get('file_util');
        /** Se o Pet vier Null apaga ficheiros do pet se existirem */
        if (!$pet) {
            $fileControl->fileDeleteTemp($request->getSession()->get('id'));
            return new Response('Wrong Information');
        }
        try {
            /** Persistir Pet */
            $em = $em->getManager();
            $em->persist($pet);
            $em->flush();
        } catch (\Exception $e) {
            $fileControl->fileDeleteTemp($request->getSession()->get('id'));
            return new Response('Failed to insert Pet');
        }

        /** upload pa cloud */
        $fileControl->fileToCloud($request->getSession()->get('id'));
        /** insere DB os links das photos */
        $fileControl->photoToDb($pet,$request->getSession()->get('id'));
        $fileControl->fileDeleteTemp($request->getSession()->get('id'));
        return new Response('Pet added!');
    }



    /***
     * @param $request
     * @param $em
     * @return Pet|null
     *  Cria um objeto Pet e valida o mesmo
     */
    private function valid_Pet($request,$em){
        $pet=new Pet();
        $currentuser = $this->get('login_check')->getCurrentUser($em,$request);
        $datenow = new DateTime($request->request->get('petbirth'));
        $pet->setBirth($datenow)
            ->setBreed($request->request->get('petbreed'))
            ->setType($request->request->get('petfamily'))
            ->setGender($request->request->get('gender'))
            ->setPetowner($currentuser);
        if(count($this->get('validator')->validate($pet))) return null;
        return $pet;

    }

}
