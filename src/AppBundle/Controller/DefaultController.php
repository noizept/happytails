<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** Se tiver logado vai para /main caso contrario mantem */
        if ($this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('main_page');
        return $this->render('base.html.twig', array('signed' => false));
    }
}