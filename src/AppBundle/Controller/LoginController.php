<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class LoginController extends Controller
{
    /**
     * @Route("/login", name="login_check")
     */
    /***
     * Controlador que valida o login
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Request $request)
    {
        /** Se já tiver Logged redirect para /main */
        if ($this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('main_page');

        /** Se o form de login tiver vazio volta po / (default) */
        elseif(empty($request->get('petfinder_password_dropdown')) ||
               empty($request->get('petfinder_email_dropdown')))
                    return $this->redirectToRoute('homepage');

        /** Verifica credenciais
         * Se tiver certas -> /main e guarda o Id na sessão
         * caso contrario vai para / (default)
         */
        else{
            $password=$request->get('petfinder_password_dropdown');
            $email=$request->get('petfinder_email_dropdown');
            $em=$this->getDoctrine()->getManager();
            $petfinder=$this->get('login_check')->validLogin($em, $email,$password);
            if(empty($petfinder))
                return $this->redirectToRoute('homepage');
            else{
                $ses=$request->getSession();
                $ses->set('id',$petfinder->getIdUser());
                $ses->save();
                return $this->redirectToRoute('main_page');
            }
        }
    }

    /**
     * @Route("/logout", name="logout_out")
     */
    /***
     * Controlador que limpa a sessão, fazendo logout
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Request $request)
    {
        $sess=$request->getSession();
        $sess->clear();
        $sess->save();
        return $this->redirectToRoute('homepage');


    }
}
