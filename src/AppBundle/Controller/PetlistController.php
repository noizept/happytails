<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Doctrine\DBAL\Query\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \DateTime;



class PetlistController extends Controller
{
    /**
     * @Route("/petlist", name="petlist")
     */
    public function indexAction(Request $request)
    {
        /** Se tiver logado vai para /main caso contrario mantem */
        if (!$this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('main_page');
        return $this->render(':List:list.html.twig', array('signed' => true));
    }


    /**
     * @Route("/listupdate", name="listupdate")
     */
    /***
     * @param Request $request
     * @return Response com a View(lista) de pets
     * Função de pesquisa na BD com base dos filtros apresentados no formulario
     */
    public function listUpdate(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Pet');

        if (empty($request->request->get('petbirth')) && empty($request->request->get('petbirth2'))) {
            $pets = $repo
                ->findBy(array('type' => $request->request->get('petfamily'),
                    'breed' => $request->request->get('petbreed')

                ));
        }

        elseif (empty($request->request->get('petbirth')) && !empty($request->request->get('petbirth2'))) {
            $date2 = new DateTime($request->request->get('petbirth2'));
            $query = $repo->createQueryBuilder('p')
                ->where('p.birth <= :birth')
                ->andWhere("p.type = :type")
                ->andWhere("p.breed=:breed")
                ->setParameters(array('birth'=>$date2,
                                        'type' => $request->request->get('petfamily'),
                                        'breed' => $request->request->get('petbreed')
                ))
                ->getQuery();
            $pets= $query->getResult();
        }

        elseif (!empty($request->request->get('petbirth')) && empty($request->request->get('petbirth2'))) {
            $date2 = new DateTime($request->request->get('petbirth'));
            $query = $repo->createQueryBuilder('p')
                ->where('p.birth >= :birth')
                ->andWhere("p.type = :type")
                ->andWhere("p.breed=:breed")
                ->setParameters(array('birth'=>$date2,
                    'type' => $request->request->get('petfamily'),
                    'breed' => $request->request->get('petbreed')
                ))
                ->getQuery();
            $pets= $query->getResult();
        }
        else{
            $date1 = new DateTime($request->request->get('petbirth'));
            $date2 = new DateTime($request->request->get('petbirth2'));
            if ($date1>$date2)
                list($date1, $date2) = array($date2, $date1);

            $query = $repo->createQueryBuilder('p')
                ->where('p.birth BETWEEN :birth AND :birth2')
                ->andWhere('p.birth <= :birth2')
                ->andWhere("p.type = :type")
                ->andWhere("p.breed=:breed")
                ->setParameters(array(
                    'birth'=>$date1,
                    'birth2'=>$date2,
                    'type' => $request->request->get('petfamily'),
                    'breed' => $request->request->get('petbreed')
                ))
                ->getQuery();
            $pets= $query->getResult();

        }
        if (empty($pets))
            return new Response("No results found for {$request->request->get('petfamily')} - {$request->request->get('petbreed')}");
            return $this->render('List/PetListing.html.twig', array('pets' => $pets));

    }
}