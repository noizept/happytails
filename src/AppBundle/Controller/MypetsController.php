<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class MypetsController extends Controller
{
    /**
     * @Route("/mypets", name="mypets")
     */
    /***
     * Load com a listagem dos pets do utilizador corrente
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** Se tiver logado vai para /main caso contrario mantem */
        if ($this->get('login_check')->checkLogin($request)) {
            $pets = $this->getPets($request->getSession()->get('id'));
            return $this->render(':Pets:Mypets.html.twig', array('signed' => true, 'pets' => $pets));
        }
        return $this->render('base.html.twig', array('signed' => false));
    }

    /***
     * @param $uid
     * @return mixed
     * Retorna Lista de Pets
     */
    private function getPets($uid){
        $em=$this->getDoctrine()->getManager();
        $petowner= $em->getRepository('AppBundle:Petfinder')->find($uid);
        return  $em->getRepository('AppBundle:Pet')->findBy(array('petowner' => $petowner));
    }
}