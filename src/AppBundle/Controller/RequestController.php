<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use AppBundle\Entity\Petrequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use Symfony\Component\HttpFoundation\Response;


class RequestController extends Controller
{
    /**
     * @Route("/request/view", name="request_view")
     */
    /***
     * @param Request $request
     * @return Response
     * Pagina de Requests inicial para vista
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('login_check')->checkLogin($request))
            return $this->render('base.html.twig', array('signed' => false));
        return $this->render('requestview.html.twig',array('signed'=>true));
    }


    /**
     * @Route("/request/add", name="request_add")
     */
    /***
     * @param Request $request
     * @return Response
     * Formulario de adição de requests
     */
    public function requestaddAction(Request $request)
    {
        if (!$this->get('login_check')->checkLogin($request))
            return $this->render('base.html.twig', array('signed' => false));
        return $this->render('requestadd.html.twig',array('signed'=>true));
    }

    /**
     * @Route("/request/create", name="request_create")
     */
    /***
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * Criação de requests
     */
    public function createAction(Request $request){
        if (!$this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('homepage');
        $Petrequest= new Petrequest();
        $doctrine=$this->getDoctrine();

        $currentuser= $doctrine->getRepository("AppBundle:Petfinder")
            ->find($request->getSession()->get('id'));
        $now=new DateTime('now');
        $Petrequest->setBreed($request->request->get('petbreed'))
                    ->setType($request->request->get('petfamily'))
                    ->setState(0)
                    ->setPetfinder($currentuser)
                    ->setRequestdate($now);

        $doctrine= $doctrine->getManager();
        $doctrine->persist($Petrequest);
        $doctrine->flush();
        return new Response();
    }

    /**
     * @Route("/request/getall", name="request_getall")
     */
    /***
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * Retorna lista de requests com base dos filtros
     */
    public function getallAction(Request $request){
        if (!$this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('homepage');
        $petrequest=$this->getDoctrine()->getRepository('AppBundle:Petrequest')
                    ->findBy(array('type' => $request->request->get('petfamily'),
                        'breed' => $request->request->get('petbreed')));
        if(empty($petrequest))
            return new Response('Nothing Found');

        return $this->render('requestlisting.html.twig',array('petrequests'=>$petrequest));

    }


}