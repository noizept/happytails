<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

class RegisterController extends Controller
{


    /**
     * @Route("/register", name="register")
     */
    public function indexAction(Request $request)
    {
        /** Se nao tiver loged, mostra formulario */
        if(!$this->get('login_check')->checkLogin($request))
            return $this->render(':Register:regist.html.twig',
                array('signed'=>false));
        else{
           return $this->redirectToRoute('homepage');
        }

    }


    /**
     * @Route("/registervalidate", name="registervalidate")
     */
    /***
     * @param Request $request
     * @return Response
     * Cria Objeto Petfinder(user), valida e insere
     */
    public function validateaction(Request $request)
    {

        $petfinder = $this->CreatePetfinder($request);

        if(count($errors=$this->get('validator')->validate($petfinder))){
            return $this->render(':Register:regist-fail.html.twig',
                array('erro'=>(string) $errors));
        }
        try{
            $em=$this->getDoctrine()->getManager();
            $em->persist($petfinder);
            $em->flush();

        } catch(\Exception $e){
            return $this->render(':Register:regist-fail.html.twig',
                array('erro'=>(string) $e)
                );
        }
        $this->forward('AppBundle:Mail:sendconfirm',array('petfinder'=>$petfinder));
        return $this->render(':Emails:regist-sucess.html.twig',
                array('petfinder_name'=>$petfinder->getUsrName())
            );

    }

    /**
     * Inserir os dados do $_POST na Entity
     * @param Request $request
     * @return Petfinder
     */
    private function CreatePetfinder(Request $request)
    {
        $petfinder = new Petfinder();
        $petfinder->setPwd($request->request->get('petfinder_password'))
            ->setCity($request->request->get('petfinder_city'))
            ->setUsrName($request->request->get('petfinder_name'))
            ->setEmail($request->request->get('petfinder_email'))
            ->setAccountType(0)
            ->setPhone($request->request->get('petfinder_phone'));
        return $petfinder;
    }

}
