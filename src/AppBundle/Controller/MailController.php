<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MailController extends Controller
{


    /***
     * Manda email ao dono do pet, dizendo que o utilizador corrente está interessado em falar sobre o animal
     * @param Request $request
     * @return Response
     */


    /**
     * @Route("/mail/contact", name="mail_contact")
     */
    public function indexAction(Request $request)
    {
        $petid=$request->request->get('idPet');
        $adopter=$this->getDoctrine()
            ->getRepository('AppBundle:Petfinder')
            ->find($request->getSession()->get('id'))
            ->getEmail();


        $owner=$this->getDoctrine()
            ->getRepository('AppBundle:Pet')
            ->find($petid)
            ->getPetowner()
            ->getEmail();

        $message = \Swift_Message::newInstance()
            ->setSubject('Someone is interested in your Pet!')
            ->setFrom('happytailsacr@gmail.com')
            ->setTo($owner)
            ->setBody(
                $this->renderView('Emails/contact.html.twig',
                    array('adopter' => $adopter,
                        'petId'=>$petid,
                        'rooturl'=>$this->getParameter('root_url')
                    )), 'text/html');
        $this->get('mailer')->send($message);
        return new Response();
    }



    /***
    /**
     * @Route("/mail/confirm", name="mail_confirm")
     */
    /***
     * Manda email a confirmar que registou no happy tails
     * @param Request $request
     * @param $petfinder
     * @return Response
     */
    public function sendconfirmAction(Request $request,$petfinder)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Welcome to Happy Tails')
            ->setFrom('happytailsacr@gmail.com')
            ->setTo($petfinder->getEmail())
            ->setBody(
                $this->renderView(':Emails:regist-sucess.html.twig',
                    array('petfinder_name' => $petfinder->getUsrName())
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return new Response();
    }

    /**
     * @Route("/mail/report", name="mail_report")
     */
    /***
     * Manda um email de report para a administraçao sobre um perfil de animal
     * @param Request $request
     * @return Response
     */
    public function reportAction(Request $request)
    {
        $petid=$request->request->get('idPet');
        $message = \Swift_Message::newInstance()
            ->setSubject('[Report] User Reported Profile')
            ->setFrom('happytailsacr@gmail.com')
            ->setTo('happytailsacr@gmail.com')
            ->setBody(
                $this->renderView('Emails/report.html.twig',
                    array('petId'=>$petid,
                        'rooturl'=>$this->getParameter('root_url')
                    )), 'text/html');
        $this->get('mailer')->send($message);
        return new Response();
    }



    /**
     * @Route("/mail/request", name="mail_request")
     */
    /***
     * Manda email ao utilizador que lançou um request, com o contato do utilizador corrente
     * @param Request $request
     * @return Response
     */
    public function contactrequest(Request $request)
    {
        $petid=$request->request->get('requestid');
        $usrmail=$this->getDoctrine()->getRepository('AppBundle:Petrequest')
            ->find($petid)
            ->getPetfinder()
            ->getEmail();

        $currentuser=$this->getDoctrine()->getRepository('AppBundle:Petfinder')
            ->find($request->getSession()->get('id'));
        $message = \Swift_Message::newInstance()
            ->setSubject('[Pet Requests] Someone might have a pet for you')
            ->setFrom('happytailsacr@gmail.com')
            ->setTo($usrmail)
            ->setBody(
                $this->renderView('Emails/request.html.twig',
                    array('currentuser'=>$currentuser,
                    )), 'text/html');
        $this->get('mailer')->send($message);
        return new Response();
    }

}