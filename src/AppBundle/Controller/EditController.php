<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pet;
use AppBundle\Entity\Petfinder;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;


class EditController extends Controller
{
    /**
     * @Route("/edit/pet/{petid}", name="edit_pet")
     */
    /***
     * Trata de dar Render no formulario de edição do Pet
     * @param Request $request
     * @param $petid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request,$petid)
    {
        $this->get('file_util')->fileDeleteTemp($request->getSession()->get('id'));
        $pet=$this->getDoctrine()
            ->getRepository('AppBundle:Pet')
            ->find($petid);
        $owner=$pet->getPetowner()
            ->getIdUser();
        if($owner!=$request->getSession()->get('id'))
            return $this->redirectToRoute('homepage');
        $photos=$this->getDoctrine()
                ->getRepository('AppBundle:Photo')
                ->findBy(array('pet'=>$pet));
        return $this->render(':PetEdit:petedit.html.twig',array(
            'signed'=>true,
            'pet'=>$pet,
            'photos'=>$photos
        ));
    }


    /**
     * @Route("/validatechange", name="validatechange")
     */
    /***
     * Valida se todos os campos a alterar no Pet estão corretos
     * e se sim, faz as alterações
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function validatechangeAction(Request $request){
        if(empty($request->getSession()->get('id')))
            return $this->redirectToRoute('homepage');
        $em=$this->getDoctrine()->getManager();
        $pet=$em->getRepository('AppBundle:Pet')
                    ->find($request->request->get('pet_id'));
        $pet=$this->petChange($request, $pet,$em);
        if(count($this->get('validator')->validate($pet)))
            new Response('Invalid Data');

        $this->get('file_util')->deletePhotoDb($this->getPhotosToRemove($request));
        $em->flush(); // tinha pet dentro
        $this->get('file_util')->fileToCloud($request->getSession()->get('id'));
        $this->get('file_util')->photoToDb($pet,$request->getSession()->get('id'));
        $this->get('file_util')->fileDeleteTemp($request->getSession()->get('id'));

        return new Response('Sucess');
    }


    /***
     * Passa os Id's das fotos a remover, para um array de Photos
     * @param Request $request
     * @return array
     * @internal param $em
     */
    private function getPhotosToRemove(Request $request){
        $photos=[];
        foreach(explode(',',$request->request->get('photos')) as $delPhoto){
            if($delPhoto)
                array_push($photos,$this->getDoctrine()->getManager()
                                                        ->getRepository('AppBundle:Photo')
                                                        ->find($delPhoto));
        }
        return $photos;
    }

    /**
     * Reformata o Pet
     * @param Request $request
     * @param Pet $pet
     * @param $em
     * @return Pet
     */
    private function petChange(Request $request, Pet $pet,ObjectManager $em)
    {
        $pet->setType($request->request->get('petfamily'));
        $pet->setGender($request->request->get('gender'));
        $pet->setBreed($request->request->get('petbreed'));
        $pet->setBirth(new DateTime($request->request->get('petbirth')));
        $newowner =null;
        $mail="";
        if(!empty($mail=$request->request->get('petowner'))){
            if(empty($newowner=$em->getRepository('AppBundle:Petfinder')
                ->findOneBy(array('email'=>$mail))))
                return null;
            $pet->setPetowner($newowner);
        }
        return $pet;
    }
}