<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Petfinder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PetviewController extends Controller
{

    /**
        * @Route("/pet/{petid}", defaults={"page": 1}, requirements={"page": "\d+"}, name="pet")
     */
    /***
     * Controlador de profile de pet
     * @param Request $request
     * @param $petid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request,$petid){
        if (!$this->get('login_check')->checkLogin($request))
            return $this->redirectToRoute('homepage');
            $pet=$this->getDoctrine()->getRepository('AppBundle:Pet')->find($petid);
            if(empty($pet))
                return $this->redirectToRoute('main_page');
            $photos=$this->getDoctrine()->getRepository('AppBundle:Photo')->findBy(array('pet'=>$pet));
            $petowner=$pet->getPetowner();
            $currentuser=$this->getDoctrine()->getRepository('AppBundle:Petfinder')->find($request->getSession()->get('id'));
        return $this->render('Pets/Pet.html.twig',array(
                'signed'=>true,
                'pet'=>$pet,
                'photos'=>$photos,
                'owner'=>$petowner,
                'currentuser'=>$currentuser
            ));
    }
}
