<?php

/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 19-11-2015
 * Time: 23:05
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @@ORM\Table(name="petfinder")
 */
class Petfinder
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idUser;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = 3,max = 70)
     */
    protected $usrName;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     * @Assert\NotBlank()
     *  @Assert\Email()
     */
    protected $email;

    /** @ORM\Column(type="string", length=70, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = 8,max = 70)
     */
    protected $pwd;

    /** @ORM\Column(type="integer", length=1, nullable=false) */
    protected $account_type;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min = 4,max = 70)
     */
    protected $city;

    /**
     * @ORM\Column(type="integer", length=9)
    *  @Assert\Regex(pattern="/\d{9}/")
     */
    protected $phone;

    

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set usrName
     *
     * @param string $usrName
     *
     * @return Petfinder
     */
    public function setUsrName($usrName)
    {
        $this->usrName = $usrName;

        return $this;
    }

    /**
     * Get usrName
     *
     * @return string
     */
    public function getUsrName()
    {
        return $this->usrName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Petfinder
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set pwd
     *
     * @param string $pwd
     *
     * @return Petfinder
     */
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;

        return $this;
    }

    /**
     * Get pwd
     *
     * @return string
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * Set accountType
     *
     * @param integer $accountType
     *
     * @return Petfinder
     */
    public function setAccountType($accountType)
    {
        $this->account_type = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return integer
     */
    public function getAccountType()
    {
        return $this->account_type;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Petfinder
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     *
     * @return Petfinder
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
