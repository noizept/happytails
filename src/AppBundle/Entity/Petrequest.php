<?php

/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 19-11-2015
 * Time: 23:05
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @@ORM\Table(name="petrequest")
 */
class Petrequest
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @ORM\Column(type="string", nullable=false) */
    protected $type;

    /** @ORM\Column(type="string", length=100, nullable=false) */
    protected $breed;

    /** @ORM\Column(type="integer", length=1, nullable=false) */
    protected $state;

    /** @ORM\Column(type="date", length=100, nullable=false) */
    protected $requestdate;

    /**
     * @ORM\ManyToOne(targetEntity="Petfinder")
     * @ORM\JoinColumn(name="petfinder", referencedColumnName="id_user")
     */
    protected $petfinder;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Petrequest
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set breed
     *
     * @param string $breed
     *
     * @return Petrequest
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * Get breed
     *
     * @return string
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Petrequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set requestdate
     *
     * @param \DateTime $requestdate
     *
     * @return Petrequest
     */
    public function setRequestdate($requestdate)
    {
        $this->requestdate = $requestdate;

        return $this;
    }

    /**
     * Get requestdate
     *
     * @return \DateTime
     */
    public function getRequestdate()
    {
        return $this->requestdate;
    }

    /**
     * Set petfinder
     *
     * @param \AppBundle\Entity\Petfinder $petfinder
     *
     * @return Petrequest
     */
    public function setPetfinder(\AppBundle\Entity\Petfinder $petfinder = null)
    {
        $this->petfinder = $petfinder;

        return $this;
    }

    /**
     * Get petfinder
     *
     * @return \AppBundle\Entity\Petfinder
     */
    public function getPetfinder()
    {
        return $this->petfinder;
    }
}
