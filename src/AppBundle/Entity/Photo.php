<?php

/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 19-11-2015
 * Time: 23:05
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @@ORM\Table(name="photo")
 */
class Photo
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @ORM\Column(type="string", length=100, nullable=false) */
    protected $original;


    /**
     * @ORM\ManyToOne(targetEntity="Pet")
     * @ORM\JoinColumn(name="pet_id", referencedColumnName="id_pet")
     */    protected $pet;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set original
     *
     * @param string $original
     *
     * @return Photo
     */
    public function setOriginal($original)
    {
        $this->original = $original;

        return $this;
    }

    /**
     * Get original
     *
     * @return string
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * Set pet
     *
     * @param \AppBundle\Entity\Pet $pet
     *
     * @return Photo
     */
    public function setPet(\AppBundle\Entity\Pet $pet = null)
    {
        $this->pet = $pet;

        return $this;
    }

    /**
     * Get pet
     *
     * @return \AppBundle\Entity\Pet
     */
    public function getPet()
    {
        return $this->pet;
    }
}
