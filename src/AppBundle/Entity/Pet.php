<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 20-11-2015
 * Time: 0:17
 */

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @@ORM\Table(name="pet")
 */
class Pet
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $idPet;

    /** @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    protected $breed;

    /** @ORM\Column(type="string", length=6, nullable=false)
     *  @Assert\NotBlank()
     */
    protected $gender;

    /** @ORM\Column(type="date", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    protected $birth;

    /** @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Petfinder")
     * @ORM\JoinColumn(name="petfinder_id", referencedColumnName="id_user")
     */
    protected $petowner;

    

    /**
     * Get idPet
     *
     * @return integer
     */
    public function getIdPet()
    {
        return $this->idPet;
    }

    /**
     * Set breed
     *
     * @param string $breed
     *
     * @return Pet
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * Get breed
     *
     * @return string
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Pet
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return Pet
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Pet
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set petowner
     *
     * @param \AppBundle\Entity\Petfinder $petowner
     *
     * @return Pet
     */
    public function setPetowner(\AppBundle\Entity\Petfinder $petowner = null)
    {
        $this->petowner = $petowner;

        return $this;
    }

    /**
     * Get petowner
     *
     * @return \AppBundle\Entity\Petfinder
     */
    public function getPetowner()
    {
        return $this->petowner;
    }
}
