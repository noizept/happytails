<?php

namespace ExtraServices\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ExtraServicesBundle:Default:index.html.twig', array('name' => $name));
    }
}
