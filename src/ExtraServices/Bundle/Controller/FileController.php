<?php

namespace ExtraServices\Bundle\Controller;

use AppBundle\Entity\Pet;
use AppBundle\Entity\Photo;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FileController extends Controller
{

    public function __construct(ContainerInterface $container,EntityManager $em)
    {
        $this->container = $container;
        $this->em=$em;
    }

    /***
     * Guarda o ficheiro do upload na pasta temporaria uploads/userID
     * @param $FILE
     * @param $userid
     * @return JsonResponse
     */
    public  function fileToTemp($FILE,$userid)
    {
        $key = md5(uniqid());
        $tmp_file_name = "{$key}.{$FILE->guessExtension()}";
        $tmp_file_path = $_SERVER['DOCUMENT_ROOT'] . "/uploads/$userid/";

        //move file
        $FILE->move($tmp_file_path, $tmp_file_name);
        return new JsonResponse([]);
    }

    /***
     * @param $userid - utilizador com as fotos temporarios
     * @return JsonResponse
     */
    public function fileToCloud($userid){
        $files=glob('uploads/'.$userid.'/*');
        foreach($files as $file) {
            if (getimagesize($file))
                $this->_uploader($file);
            else
                unlink($file);
        }
        return new JsonResponse([]);
    }

    /***
     * Remove ficheiros temporarios do User
     * @param $userid
     */
    public function fileDeleteTemp($userid){
        $files=glob('uploads/'.$userid.'/*');
        foreach($files as $file){
            unlink($file);
        }
    }

    /***
     * Faz upload para a cloud
     * @param $file
     * @return array
     */
    private function _uploader($file)
    {
        $key = $this->container->getParameter('smartfile_key');
        $secret = $this->getParameter('smartfile_secret');
        $uploader = new \Smartfile_Smartfile($key, $secret);
        $uploader->api_base_url = 'https://noizept.smartfile.com/api/2';
        $f2up = fopen($file, 'rb');
        $uploader->post('/path/data/', array($file => $f2up));
        fclose($f2up);
    }


    /***
     * @param Pet $pet
     * @param $userid
     */
    public function photoToDb(Pet $pet,$userid){
        $images=glob('uploads/'.$userid.'/*');
        if(empty($images)) return;
        foreach($images as $image){
            $photo=new Photo();
            $photo->setPet($pet);
            $filename=explode('/',$image);
            $photo->setOriginal('https://file.ac/BKBqyyN2RQw/'.$filename[2]);
            $this->getDoctrine()->getManager()->persist($photo);
            $this->getDoctrine()->getManager()->flush();
        }
    }

    /***
     * Recebe o Respositorio e o array de Photos e apaga as mesmas
     * @param $photos
     * @internal param $em
     */
    public function deletePhotoDb($photos){
        foreach($photos as $photo) {
            if($photo) {
                $this->getDoctrine()->getManager()->remove($photo);
            }
        }
        $this->getDoctrine()->getManager()->flush();
    }

}
