<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 21-11-2015
 * Time: 17:10
 */

namespace ExtraServices\Bundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Petfinder;

class LoginChecker extends Controller
{

    /***
     * Verifica sessao se tem o ID do utilizador autenticado
     * @param Request $request
     * @return bool
     */
    public  function checkLogin(Request $request){
        if(empty($request->getSession()->get('id')))
            return false;
        else
            return true;
    }

    /***
     * Verifica na BD se o login esta correto
     * @param $email email do utilizador
     * @param $password password to utilizador
     *
     */
    public function validLogin($em,$email,$password){
        $query = $em->createQuery(
            'SELECT p
                 FROM AppBundle:Petfinder p
                 WHERE p.email = :email AND p.pwd = :password
                 '
        )->setParameter('email', $email)->setParameter('password',$password);
        return $query->setMaxResults(1)->getOneOrNullResult();

    }

    public function getCurrentUser($em,$request){
        return $em->getRepository('AppBundle:Petfinder')->find($request->getSession()->get('id'));

    }

}