<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('Smartfile_',__DIR__.'/../vendor/smartfile/lib');

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
